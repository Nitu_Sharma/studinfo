# Generated by Django 2.0.1 on 2019-04-16 10:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('classroom', '0002_remove_subject_color'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Subject',
        ),
    ]

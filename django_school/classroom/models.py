from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.html import escape, mark_safe
from phone_field import PhoneField

class User(AbstractUser):
    is_student = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)


# class Subject(models.Model):
#     name = models.CharField(max_length=30)
    
#     def __str__(self):
#         return self.name


class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    #attenence = models.OneToOneField(Attendence)

    def __str__(self):
        return self.user.username

class Teacher(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return self.user.username

# class Attendence(models.Model):
#     user=models.ForeignKey(User, on_delete=models.CASCADE, primary_key=True)
#     attendence=models.C


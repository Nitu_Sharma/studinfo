from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from django.forms.utils import ValidationError

from classroom.models import ( Student,Teacher, User)


class TeacherSignUpForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields=('username','first_name','last_name','email',)

    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_teacher = True
        if commit:
            user.save()

        teacher = Teacher.objects.create(user=user)
        return user


class StudentSignUpForm(UserCreationForm):
   
   class Meta(UserCreationForm.Meta):
        model = User
        fields=('username','first_name','last_name','email',)

   @transaction.atomic
   def save(self):
        user = super().save(commit=False)
        user.is_student = True
        user.save()
        student = Student.objects.create(user=user)
        
        return user

